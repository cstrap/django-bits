# Django Must Have #


```
#!bash

$ virtualenv django-must-have
$ cd django-must-have
$ . ./bin/activate
$ pip install -r requirements.txt
```

Some of useful app packages to use in django projects.